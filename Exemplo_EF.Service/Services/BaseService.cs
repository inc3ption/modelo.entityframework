﻿using Exemplo_EF.Data.Repository;
using Exemplo_EF.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Service.Services
{
    public class BaseService<T> where T : _BaseEntity
    {
        private EntityRepository<T> repositorio = new EntityRepository<T>();

        public void Salvar(T obj)
        {
            try
            {
                repositorio.Save(obj);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remover(int id)
        {
            try
            {
                var cliente = repositorio.GetById(id);

                repositorio.Delete(cliente);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<T> RecuperarTodos()
        {
            try
            {
                return repositorio.GetAll();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public T RecuperarPorId(int id)
        {
            try
            {
                return repositorio.GetById(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
