﻿using Exemplo_EF.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Domain.Interfaces
{
    public interface IRepository<T> where T : _BaseEntity
    {
        void Save(T obj);
        void Delete(T obj);
        T GetById(object id);
        IList<T> GetAll();
        IList<T> GetManyBySpecification(Func<T, bool> predicate);
        T GetBySpecification(Func<T, bool> predicate);
    }
}
