﻿using Exemplo_EF.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Domain.Entities
{
    public class Contato : _BaseEntity
    {
        public string Conteudo { get; set; }
        public TipoContato TipoContato { get; set; }
        public int? ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
