﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Domain.Entities
{
    public class Cliente : _BaseEntity
    {
        public Cliente()
        {
            ContatoLista = new List<Contato>();
        }

        public string Nome { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string Cpf { get; set; }

        public virtual ICollection<Contato> ContatoLista { get; set; }
    }
}
