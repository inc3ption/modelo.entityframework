﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Domain.Entities
{
    public class _BaseEntity
    {
        public int Id { get; set; }
    }
}
