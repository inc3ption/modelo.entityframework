﻿using Exemplo_EF.Domain.Entities;
using Exemplo_EF.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Exemplo_EF.Application.Controllers
{
    [RoutePrefix("api/contato")]
    public class ContatoController : ApiController
    {
        private BaseService<Contato> service = new BaseService<Contato>();

        [HttpPost]
        [Route("save")]
        public async Task<IHttpActionResult> Save([FromBody] Contato obj)
        {
            try
            {
                service.Salvar(obj);

                return await Task.Run(function: () => Ok());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateResponse(ex.Message));
            }
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                service.Remover(id);

                return await Task.Run(() => Ok());
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateResponse(ex.Message));
            }
        }

        [HttpGet]
        [Route("getall")]
        public async Task<IHttpActionResult> GetAll()
        {
            try
            {
                return await Task.Run(() => Ok(service.RecuperarTodos()));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateResponse(ex.Message));
            }
        }

        [HttpGet]
        [Route("getbyid/{id}")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            try
            {
                return await Task.Run(() => Ok(service.RecuperarPorId(id)));
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateResponse(ex.Message));
            }
        }
    }
}
