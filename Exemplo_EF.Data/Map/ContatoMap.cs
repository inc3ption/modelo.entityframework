﻿using Exemplo_EF.Domain.Entities;
using Exemplo_EF.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Data.Map
{
    public class ContatoMap : EntityTypeConfiguration<Contato>, IMap
    {
        public ContatoMap()
        {
            ToTable("Contato");

            HasKey(c => c.Id);

            Property(c => c.Id).IsRequired().HasColumnName("Id");
            Property(c => c.Conteudo).IsRequired().HasColumnName("Conteudo");
            Property(c => c.TipoContato).IsRequired().HasColumnName("TipoContato");
            Property(c => c.ClienteId).IsOptional().HasColumnName("ClienteId");
        }
    }
}
