﻿using Exemplo_EF.Domain.Entities;
using Exemplo_EF.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Data.Map
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>, IMap
    {
        public ClienteMap()
        {
            ToTable("Cliente");

            HasKey(c => c.Id);

            Property(c => c.Id).IsRequired().HasColumnName("Id");
            Property(c => c.Cpf).IsRequired().HasColumnName("Cpf");
            Property(c => c.DataNascimento).IsOptional().HasColumnName("DataNascimento");
            Property(c => c.Nome).IsRequired().HasColumnName("Nome");
        }
    }
}
