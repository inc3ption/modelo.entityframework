﻿using Exemplo_EF.Data.Context;
using Exemplo_EF.Domain.Entities;
using Exemplo_EF.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exemplo_EF.Data.Repository
{
    public class EntityRepository<T> : IDisposable, IRepository<T> where T : _BaseEntity
    {
        private EntityContext<T> context;

        public EntityRepository() => context = new EntityContext<T>();

        public void Save(T obj)
        {
            using (var contextTransact = context.Database.BeginTransaction())
            {
                try
                {
                    if (obj.Id == 0)
                        context.Set<T>().Add(obj);
                    else
                        context.Entry(obj).State = EntityState.Modified;

                    context.SaveChanges();

                    contextTransact.Commit();
                }
                catch (Exception ex)
                {
                    contextTransact.Rollback();

                    throw new Exception(ex.Message);
                }
            }
        }

        public void Delete(T obj)
        {
            using (var contextTransact = context.Database.BeginTransaction())
            {
                try
                {
                    context.Set<T>().Remove(obj);

                    contextTransact.Commit();
                }
                catch (Exception ex)
                {
                    contextTransact.Rollback();

                    throw new Exception(ex.Message);
                }
            }
        }

        public IList<T> GetAll()
        {
            try
            {
                return context.Set<T>().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public T GetById(object id)
        {
            try
            {
                return context.Set<T>().Where(c => c.Id == (int)id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public T GetBySpecification(Func<T, bool> predicate)
        {
            try
            {
                return context.Set<T>().Where(predicate).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<T> GetManyBySpecification(Func<T, bool> predicate)
        {
            try
            {
                return context.Set<T>().Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
